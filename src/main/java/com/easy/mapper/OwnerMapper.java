package com.easy.mapper;

import com.easy.entity.Owner;

public interface OwnerMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Owner record);

    int insertSelective(Owner record);

    Owner selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Owner record);

    int updateByPrimaryKey(Owner record);
    
    Owner selectByUsername(String username);
}