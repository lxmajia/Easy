package com.easy.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.easy.entity.User;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

	User findUserByMobile(String mobile);
	
	int getUserCount();
	
	List<User> getUsersListByPage(@Param("startrow")Integer startrow, @Param("size")Integer size);
	
	List<User> getAll();
	
	List<User> getLiaoXiangUser();
}