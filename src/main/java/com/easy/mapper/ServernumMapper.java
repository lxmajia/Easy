package com.easy.mapper;

import java.util.List;

import com.easy.entity.Servernum;

public interface ServernumMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Servernum record);

    int insertSelective(Servernum record);

    Servernum selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Servernum record);

    int updateByPrimaryKey(Servernum record);
    
    Servernum selectServernumLessTime();
 
    List<Servernum> getAll();
}