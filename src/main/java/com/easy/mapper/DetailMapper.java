package com.easy.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.easy.entity.Detail;

public interface DetailMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Detail record);

    int insertSelective(Detail record);

    Detail selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Detail record);

    int updateByPrimaryKey(Detail record);
    
    List<Detail> getDetaiListByUserId(@Param("userid")Integer userid);
    
    List<Detail> getDetailToDo(@Param("userid")Integer userid);
    
    Detail userIsBan(@Param("userid")Integer userid);
    
    List<Detail> getAllDetailByContent(@Param("content")String content);
    
    List<Detail> getDetailsListByPage(@Param("startrow")Integer startrow, @Param("size")Integer size);
    
    int getDetailsCount();

	List<Detail> getAllDetailByCheckflagByPage(@Param("checkflag")Integer checkflag,@Param("startrow")Integer startrow, @Param("size")Integer size);
    
	int getDetailsNoCheckflagCount(@Param("checkflag")Integer checkflag);

	void deleteAll();

	Detail getDetailByMobile(@Param("phone")String phone);
}