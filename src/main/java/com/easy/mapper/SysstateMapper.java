package com.easy.mapper;

import com.easy.entity.Sysstate;

public interface SysstateMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Sysstate record);

    int insertSelective(Sysstate record);

    Sysstate selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Sysstate record);

    int updateByPrimaryKey(Sysstate record);
}