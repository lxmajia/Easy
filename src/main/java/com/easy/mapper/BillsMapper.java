package com.easy.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.easy.entity.Bills;

public interface BillsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Bills record);

    int insertSelective(Bills record);

    Bills selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Bills record);

    int updateByPrimaryKey(Bills record);

	List<Bills> getBillListByPage(@Param("startrow")Integer startrow, @Param("size")Integer size);

	int getBillsCount();
}