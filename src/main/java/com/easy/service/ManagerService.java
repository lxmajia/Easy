package com.easy.service;

import java.util.List;

import com.easy.entity.Owner;
import com.easy.entity.Servernum;
import com.easy.entity.Sysstate;

public interface ManagerService {

	 Owner getOwnerByUsername(String username);
	 
	 Sysstate getSysstate();
	 
	 void updateSysstate(Sysstate sys);
	
	 void updateOwner(Owner owner);
	 
	 List<Servernum> getAllList();
	 
	 Servernum getServerNumById(Integer id);
	 
	 void updateServerNum(Servernum server);
	 
	 void deleteServernum(Integer id);
	 
	 void addServernum(Servernum server);
}
