package com.easy.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.easy.entity.Detail;
import com.easy.entity.Sysstate;
import com.easy.entity.User;

public interface UserService {

	User getUserById(Integer id);
	
	User getUserByMobile(String mobile);

	int getUserCount();
	
	List<User> getUsersListByPage(Integer pagenow, int i);
	
	void updateUser(User user);
	
	Sysstate getSysstate();
	
	void addUser(User user);
	
	List<User> getAll();
	
	void deleteUser(Integer uid);
}
