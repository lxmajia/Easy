package com.easy.service;

public interface ApiService {

	String getPhone(Integer detailid);

	String getSmscode(Integer detailid);

	String finishTask(Integer detailid);

	String domore(Integer detailid);

	String doagain(Integer detailid);

	String refreshLogin();
}
