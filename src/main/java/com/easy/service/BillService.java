package com.easy.service;

import java.util.List;

import com.easy.entity.Bills;
import com.easy.entity.Detail;

public interface BillService {
	void detailToBill(Detail detail,Integer userid,String billtype,String money);
	
	List<Bills> getBillListByPage(Integer pagenow, int i);
	
	int getBillsCount();
}
