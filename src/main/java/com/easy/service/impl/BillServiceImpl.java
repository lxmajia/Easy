package com.easy.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.easy.entity.Bills;
import com.easy.entity.Detail;
import com.easy.mapper.BillsMapper;
import com.easy.service.BillService;

@Service
public class BillServiceImpl implements BillService {

	@Resource
	private BillsMapper billMapper;
	@Override
	public void detailToBill(Detail d,Integer userid,String billtype,String money) {
		Bills bill = new Bills();
		bill.setApplytime(d.getApplytime());
		bill.setBillby(1);
		bill.setBilltime(new Date());
		bill.setBilltype(billtype);
		bill.setDetailid(d.getId());
		bill.setMoney(money);
		bill.setOvertime(d.getOvertime());
		bill.setPhone(d.getPhone());
		bill.setSecurity(d.getSecurity().toString());
		bill.setSerialid(d.getSerialid());
		bill.setSmscode(d.getSmscode());
		bill.setSuretime(d.getSuretime());
		bill.setToken(d.getToken());
		bill.setUserid(d.getUserid());
		billMapper.insertSelective(bill);
	}

	@Override
	public List<Bills> getBillListByPage(Integer pagenow, int i) {
		int startrow = (pagenow-1)*i;
		List<Bills> bills = billMapper.getBillListByPage(startrow, i);
		return bills;
	}

	@Override
	public int getBillsCount() {
		return billMapper.getBillsCount();
	}

}
