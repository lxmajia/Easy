package com.easy.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.easy.entity.Detail;
import com.easy.entity.User;
import com.easy.mapper.DetailMapper;
import com.easy.service.DetailService;

@Service
public class DetailServiceImpl implements DetailService {

	@Resource
	private DetailMapper detailMapper;

	@Override
	public List<Detail> getDetaiListByUserId(Integer userid) {
		return detailMapper.getDetaiListByUserId(userid);
	}

	@Override
	public List<Detail> getDetailToDo(Integer userid) {
		return detailMapper.getDetailToDo(userid);
	}

	@Override
	public void createtask(Detail detail) {
		detail.setApplytime(new Date());
		detail.setGetphone((byte) 0);
		detailMapper.insertSelective(detail);
	}

	@Override
	public Detail userIsBan(Integer userid) {
		return detailMapper.userIsBan(userid);
	}

	@Override
	public List<Detail> getAllDetailByContent(String content) {
		return detailMapper.getAllDetailByContent(content);
	}

	@Override
	public int getDetailsCount() {
		return detailMapper.getDetailsCount();
	}

	@Override
	public List<Detail> getDetailsListByPage(Integer pagenow, int i) {
		int startrow = (pagenow - 1) * i;
		List<Detail> detailsList = detailMapper.getDetailsListByPage(startrow, i);
		return detailsList;
	}

	@Override
	public Detail getDetailById(Integer did) {
		return detailMapper.selectByPrimaryKey(did);
	}

	@Override
	public List<Detail> getAllDetailByCheckedByPage(Integer checked, Integer pagenow, int i) {
		int startrow = (pagenow - 1) * i;
		List<Detail> allDetailByCheckedByPage = detailMapper.getAllDetailByCheckflagByPage(checked, startrow, i);
		return allDetailByCheckedByPage;
	}

	@Override
	public int getDetailsCheckedCount(Integer checked) {
		return detailMapper.getDetailsNoCheckflagCount(checked);
	}

	@Override
	public void updateDetail(Detail d) {
		detailMapper.updateByPrimaryKeySelective(d);
	}

	@Override
	public boolean finishToday(User user) {
		List<Detail> detaiListByUserId = detailMapper.getDetaiListByUserId(user.getId());
		Detail userIsBan = detailMapper.userIsBan(user.getId());
		if (userIsBan != null) {
			return true;
		} else {
			if (detaiListByUserId.size() == user.getAllowcount()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void deleteAll() {
		detailMapper.deleteAll();
	}

	@Override
	public void deleteDetail(Integer did) {
		detailMapper.deleteByPrimaryKey(did);
	}

	@Override
	public Detail getDetailByMobile(String mobile) {
		return detailMapper.getDetailByMobile(mobile);
	}

}
