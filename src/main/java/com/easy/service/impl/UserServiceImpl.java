package com.easy.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.easy.entity.Detail;
import com.easy.entity.Sysstate;
import com.easy.entity.User;
import com.easy.mapper.SysstateMapper;
import com.easy.mapper.UserMapper;
import com.easy.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Resource
	private UserMapper userMapper;
	@Resource
	private SysstateMapper sysMapper;
	
	@Override
	public User getUserById(Integer id) {
		return userMapper.selectByPrimaryKey(id);
	}

	@Override
	public User getUserByMobile(String mobile) {
		return userMapper.findUserByMobile(mobile);
	}

	@Override
	public int getUserCount() {
		return userMapper.getUserCount();
	}

	@Override
	public List<User> getUsersListByPage(Integer pagenow, int i) {
		int startrow = (pagenow-1)*i;
		List<User> users = userMapper.getUsersListByPage(startrow, i);
		return users;
	}

	@Override
	public void updateUser(User user) {
		userMapper.updateByPrimaryKeySelective(user);
	}

	@Override
	public Sysstate getSysstate() {
		return sysMapper.selectByPrimaryKey(1);
	}

	@Override
	public void addUser(User user) {
		userMapper.insertSelective(user);
	}

	@Override
	public List<User> getAll() {
		return userMapper.getAll();
	}

	@Override
	public void deleteUser(Integer uid) {
		userMapper.deleteByPrimaryKey(uid);
	}
	
}
