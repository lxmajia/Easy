package com.easy.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.easy.entity.Owner;
import com.easy.entity.Servernum;
import com.easy.entity.Sysstate;
import com.easy.mapper.OwnerMapper;
import com.easy.mapper.ServernumMapper;
import com.easy.mapper.SysstateMapper;
import com.easy.service.ManagerService;

@Service
public class ManagerServiceImpl implements ManagerService {

	@Resource
	private OwnerMapper ownerMapper;
	@Resource
	private SysstateMapper sysMapper;
	@Resource
	private ServernumMapper numMapper;
	
	public Owner getOwnerByUsername(String username){
		return ownerMapper.selectByUsername(username);
	}

	@Override
	public Sysstate getSysstate() {
		return sysMapper.selectByPrimaryKey(1);
	}

	@Override
	public void updateSysstate(Sysstate sys) {
		sysMapper.updateByPrimaryKeySelective(sys);
	}

	@Override
	public void updateOwner(Owner owner) {
		ownerMapper.updateByPrimaryKeySelective(owner);
	}

	@Override
	public List<Servernum> getAllList() {
		return numMapper.getAll();
	}

	@Override
	public Servernum getServerNumById(Integer id) {
		return numMapper.selectByPrimaryKey(id);
	}

	@Override
	public void updateServerNum(Servernum server) {
		numMapper.updateByPrimaryKeySelective(server);
	}

	@Override
	public void deleteServernum(Integer id) {
		numMapper.deleteByPrimaryKey(id);
	}

	@Override
	public void addServernum(Servernum server) {
		numMapper.insertSelective(server);
	}
	
	
	
}
