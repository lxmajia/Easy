package com.easy.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.easy.entity.Detail;
import com.easy.entity.Servernum;
import com.easy.mapper.DetailMapper;
import com.easy.mapper.ServernumMapper;
import com.easy.service.ApiService;
import com.easy.utils.ApiUtils;

@Service
public class ApiServiceImpl implements ApiService {

	@Resource
	private ServernumMapper serverMapper;

	@Resource
	private DetailMapper detailMapper;

	public String refreshLogin() {
		String result;
		List<Servernum> allapinum = serverMapper.getAll();
		for (Servernum num : allapinum) {
			result = ApiUtils.getLogin(num.getApinum(), num.getApipwd());
			String[] login = result.split("\\|");
			if (login[0].equals("1")) {
				if (!login[1].equals(num.getToken())) {
					num.setToken(login[1]);
					num.setTokentime(new Date());
					num.setInusing((byte) 0);
					num.setIsban("0");
					serverMapper.updateByPrimaryKeySelective(num);
				}
			} else {
				num.setToken("nosuccessfortoken");
				num.setTokentime(new Date());
				num.setInusing((byte) 0);
				num.setIsban("1");
				serverMapper.updateByPrimaryKeySelective(num);
			}
		}
		return "";
	}

	@Override
	public String getPhone(Integer detailid) {
		Detail detail = detailMapper.selectByPrimaryKey(detailid);
		if (detail.getGetphone().equals(1)) {
			return "hava_phone";
		}
		// 选择一个使用次数最少的服务器账户
		Servernum servernum = serverMapper.selectServernumLessTime();

		if (servernum == null) {
			return "num_error";
		} else {
			detail.setSecurity(servernum.getId());
			detail.setToken(servernum.getToken());
			String getphone = ApiUtils.getPhone(detail.getToken());
			String[] phone = getphone.split("\\|");
			if (phone[0].equals("1")) {
				detail.setPhone(phone[1]);
				detail.setState("0");
				detail.setGetphone((byte) 1);
				detail.setCheckflag(0);
				detailMapper.updateByPrimaryKeySelective(detail);
				return phone[1];
			} else {
				return "phone_error";
			}
		}
	}

	@Override
	public String getSmscode(Integer detailid) {
		Detail detail = detailMapper.selectByPrimaryKey(detailid);
		if (detail.getState().equals(1)) {
			return "hava_code";
		}
		String smsCode;
		String[] code;
		for (int i = 0; i < 20; i++) {
			try {
				Thread.sleep(3000);
				smsCode = ApiUtils.getSmsCode(detail.getPhone(), detail.getToken());
				code = smsCode.split("\\|");
				if (code[0].equals("1")) {
					String smscode = code[1].substring(code[1].indexOf("验证码") + 3, code[1].indexOf("。"));
					detail.setSmscode(smscode);
					detail.setState("1");
					detail.setCheckflag(0);
					detailMapper.updateByPrimaryKeySelective(detail);
					return smscode;
				} else {
					continue;
				}
			} catch (InterruptedException e) {
				return "sleep_error";
			}
		}
		ApiUtils.addBlacklist(detail.getPhone(), detail.getToken());
		detailMapper.deleteByPrimaryKey(detailid);
		return "phone_black";
	}

	@Override
	public String finishTask(Integer detailid) {
		Detail detail = detailMapper.selectByPrimaryKey(detailid);
		if (detail.getPhone().isEmpty()) {
			return "no_phone";
		}
		if (detail.getSmscode().isEmpty()) {
			return "no_code";
		}
		// 处理状态
		detail.setCheckflag(1);
		detail.setOvertime(new Date());
		detail.setContent("success");
		detailMapper.updateByPrimaryKeySelective(detail);
		// 处理token数量
		Servernum number = serverMapper.selectByPrimaryKey(detail.getSecurity());
		number.setInusing((byte) (number.getInusing() - 1));
		serverMapper.updateByPrimaryKeySelective(number);
		return "success";
	}

	@Override
	public String domore(Integer detailid) {
		Detail detail = detailMapper.selectByPrimaryKey(detailid);
		if (detail.getPhone().isEmpty()) {
			return "no_phone";
		}
		if (detail.getSmscode().isEmpty()) {
			return "no_code";
		}
		detail.setCheckflag(2);
		detail.setOvertime(new Date());
		detail.setSuretime(new Date());
		detail.setContent("ban");
		detailMapper.updateByPrimaryKeySelective(detail);
		// 处理token数量
		Servernum number = serverMapper.selectByPrimaryKey(detail.getSecurity());
		number.setInusing((byte) (number.getInusing() - 1));
		serverMapper.updateByPrimaryKeySelective(number);
		return "success";
	}

	@Override
	public String doagain(Integer detailid) {
		Detail detail = detailMapper.selectByPrimaryKey(detailid);
		if (detail.getPhone().isEmpty()) {
			return "no_phone";
		}
		if (detail.getSmscode().isEmpty()) {
			return "no_code";
		}
		detail.setCheckflag(2);
		detail.setOvertime(new Date());
		detail.setSuretime(new Date());
		detail.setContent("havado");
		detailMapper.updateByPrimaryKeySelective(detail);
		// 处理token数量
		Servernum number = serverMapper.selectByPrimaryKey(detail.getSecurity());
		number.setInusing((byte) (number.getInusing() - 1));
		serverMapper.updateByPrimaryKeySelective(number);
		return "success";
	}

}
