package com.easy.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.easy.entity.Detail;
import com.easy.entity.User;

public interface DetailService {

	public List<Detail> getDetaiListByUserId(Integer userid);
	
	List<Detail> getDetailToDo(Integer userid);
	
	void createtask(Detail detail);
	
	Detail userIsBan(Integer userid);
	
	List<Detail> getAllDetailByContent(String content);
	
	int getDetailsCount();

	List<Detail> getDetailsListByPage(Integer pagenow, int i);

	Detail getDetailById(Integer did);
	
	List<Detail> getAllDetailByCheckedByPage(Integer checked,Integer pagenow, int i);
	
	int getDetailsCheckedCount(Integer checked);
	
	void updateDetail(Detail d);
	
	boolean finishToday(User user);
	
	void deleteAll();

	void deleteDetail(Integer did);

	Detail getDetailByMobile(String mobile);
}
