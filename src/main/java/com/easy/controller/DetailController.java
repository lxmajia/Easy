package com.easy.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.shiro.web.servlet.ShiroHttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.easy.entity.Detail;
import com.easy.entity.User;
import com.easy.service.DetailService;

@Controller
@RequestMapping(value="/detail")
public class DetailController {
	
	@Autowired
	private DetailService detailService;
	
	@RequestMapping(value="/mytask.htm")
	public String mytask(Model model,HttpSession session){
		User user = (User)session.getAttribute("userinfo");
		List<Detail> detaillist = detailService.getDetaiListByUserId(user.getId());
		model.addAttribute("detaillist", detaillist);
		return "mytask";
	}
	
	@RequestMapping(value="/dotask.htm")
	public String dotask(Model model,HttpSession session){
		User user = (User)session.getAttribute("userinfo");
		model.addAttribute("title", "添加任务提示");
		Detail d = detailService.userIsBan(user.getId());
		if(d!=null){
			model.addAttribute("msg", "你的账号出现绑定频繁的情况，今天不能申请！请明天再来！");
			return "result_page";
		}
		
		List<Detail> detaillist = detailService.getDetailToDo(user.getId());
		if(detaillist.size()>1){
			model.addAttribute("msg", "你的账号出现异常，请联系管理员。错误码：too_more");
			return "result_page";
		}else if(detaillist.size()==1){
			model.addAttribute("detail", detaillist.get(0));
			return "dotask";
		}else{
			return "newtask";
		}
	}
	
	@RequestMapping(value="/createtask.htm")
	public String createtask(Model model,HttpSession session){
		model.addAttribute("title", "任务提醒");
		String msg = "";
		User user = (User)session.getAttribute("userinfo");

		Detail detail = new Detail();
		List<Detail> detailToDo = detailService.getDetailToDo(user.getId());
		List<Detail> detaiList = detailService.getDetaiListByUserId(user.getId());
		int size = detaiList.size();
		if(detailToDo.size()==0){
			if(size<user.getAllowcount()){
				detail.setUserid(user.getId());
				detail.setCheckflag(0);
				detail.setSerialid(size+1);
				detailService.createtask(detail);
			}else{
				msg = "当天已经<red style='color:red;'>申请次数已经达到限制</red>，不能申请！";
				model.addAttribute("msg", msg);
				return "result_page";
			}
		}else{
		}
		return "redirect:/detail/dotask.htm";
	}
}
