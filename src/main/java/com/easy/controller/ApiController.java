package com.easy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easy.service.ApiService;
import com.easy.service.DetailService;

@Controller
@RequestMapping("/api")
public class ApiController {

	@Autowired
	private ApiService apiService;
	@Autowired
	private DetailService detailService;

	@RequestMapping("/getPhone.htm")
	public String getPhone(Integer id, Model model) {
		model.addAttribute("title", "获取手机号");
		String resule = apiService.getPhone(id);
		if (resule.equals("num_error")) {
			// 数据库没有账号了
			model.addAttribute("msg", "获取失败，错误码：num_error");
		} else if (resule.equals("size_error")) {
			// 数据库的所有账号使用人数都已经达到十个了
			model.addAttribute("msg", "获取失败，错误码：size_error");
		} else if (resule.equals("phone_error")) {
			// 获取手机号码失败，标志位为0，可能原因1系统暂时没有可用号码，请过3秒再重新取号，2余额不足，3超出频率，请延时3秒再请求。
			model.addAttribute("msg", "获取失败，错误码：phone_error");
		} else if (resule.equals("hava_phone")) {
			// 提供的账号登录获取token时不成功，标志位为0
			model.addAttribute("msg", "请勿重复获取！");
		} else {
			model.addAttribute("msg", resule);
		}
		return "redirect:/detail/dotask.htm";
	}

	@RequestMapping("/getSmscode.htm")
	public String getSmscode(Integer id, Model model) {
		model.addAttribute("title", "获取验证码");
		String result = apiService.getSmscode(id);
		if (result.equals("hava_code")) {
			model.addAttribute("msg", "获取失败，错误码：hava_code");
			return "result_page";
		} else if (result.equals("sleep_error")) {
			// 数据库没有账号了
			model.addAttribute("msg", "获取失败，错误码：sleep_error");
			return "result_page";
		} else if (result.equals("phone_black")) {
			// 一分钟没有取到，已经将账号拉黑
			model.addAttribute("msg", "规定时间内获取失败，此号码系统判定为作废，请重新获取，错误码：phone_black");
			return "result_page";
		} else {
			model.addAttribute("msg", result);
			return "redirect:/detail/dotask.htm";
		}
	}

	@RequestMapping("/getSmscodeAsyc.htm")
	@ResponseBody
	public String getSmscodeAsyc(Integer id) {
		String result = apiService.getSmscode(id);
		if (result.equals("hava_code")) {
			return "result_page";
		} else if (result.equals("sleep_error")) {
			return "result_page";
		} else if (result.equals("phone_black")) {
			return "result_page";
		} else {
			return result;
		}
	}

	@RequestMapping("/finishtask.htm")
	public String finishtask(Integer id, Model model) {
		model.addAttribute("title", "完成状态");
		String result = apiService.finishTask(id);
		if (result.equals("no_phone")) {
			// 之前没有获取到电话号码
			model.addAttribute("msg", "获取失败，错误码：no_phone");
		} else if (result.equals("no_code")) {
			// 没有获取到验证码
			model.addAttribute("msg", "获取失败，错误码：no_code");
		} else if (result.equals("success")) {
			// 成功
			model.addAttribute("msg", "此业务完成！");
			return "redirect:/detail/mytask.htm";
		}
		return "result_page";
	}

	@RequestMapping("/domore.htm")
	public String domore(Integer id, Model model) {
		model.addAttribute("title", "完成状态");
		String result = apiService.domore(id);
		if (result.equals("no_phone")) {
			// 之前没有获取到电话号码
			model.addAttribute("msg", "获取失败，错误码：no_phone");
		} else if (result.equals("no_code")) {
			// 没有获取到验证码
			model.addAttribute("msg", "获取失败，错误码：no_code");
		} else if (result.equals("success")) {
			// 成功
			model.addAttribute("msg", "已经上报为：系统返回频繁！出现此信息此账号今天不能再次完成任务！");
		}
		return "result_page";
	}

	@RequestMapping("/doagain.htm")
	public String doagain(Integer id, Model model) {
		model.addAttribute("title", "完成状态");
		String result = apiService.doagain(id);
		if (result.equals("no_phone")) {
			// 之前没有获取到电话号码
			model.addAttribute("msg", "获取失败，错误码：no_phone");
		} else if (result.equals("no_code")) {
			// 没有获取到验证码
			model.addAttribute("msg", "获取失败，错误码：no_code");
		} else if (result.equals("success")) {
			// 成功
			model.addAttribute("msg", "已经上报为：系统返回已被绑定！此次任务不作结算！");
		}
		return "result_page";
	}

}
