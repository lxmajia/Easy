package com.easy.controller;

import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.easy.entity.Sysstate;
import com.easy.entity.User;
import com.easy.service.UserService;

@Controller
public class CommonController {

	@Autowired
	private UserService userService;

	@RequestMapping("/login.htm")
	public String login(Model model) {
		Sysstate sysstate = userService.getSysstate();
		if (sysstate.getWorking().equals("1")) {
			return "login";
		} else {
			model.addAttribute("content", sysstate.getContent());
			return "sysclose";
		}
	}

	@RequestMapping("/superlogin.htm")
	public String superlogin(Model model) {
		return "login";
	}

	@RequestMapping("/logout.htm")
	public String logout() {
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		return "redirect:/login.htm";
	}

	@RequestMapping("/index.htm")
	public String index(Model model, HttpSession session) {
		User user = (User) session.getAttribute("userinfo");
		model.addAttribute("user", user);
		return "index";
	}

	@RequestMapping("/404.htm")
	public String err404() {
		return "err404";
	}

	@RequestMapping("/managerlogin.htm")
	public String managerlogin() {
		return "manager/login";
	}
}
