package com.easy.entity;

import java.util.Date;

public class Sysstate {
    private Integer id;

    private String sysinfo;

    private String working;

    private Date changetime;

    private String content;

    private Integer changeby;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSysinfo() {
        return sysinfo;
    }

    public void setSysinfo(String sysinfo) {
        this.sysinfo = sysinfo == null ? null : sysinfo.trim();
    }

    public String getWorking() {
        return working;
    }

    public void setWorking(String working) {
        this.working = working == null ? null : working.trim();
    }

    public Date getChangetime() {
        return changetime;
    }

    public void setChangetime(Date changetime) {
        this.changetime = changetime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getChangeby() {
        return changeby;
    }

    public void setChangeby(Integer changeby) {
        this.changeby = changeby;
    }
}