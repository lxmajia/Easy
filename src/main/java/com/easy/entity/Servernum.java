package com.easy.entity;

import java.util.Date;

public class Servernum {
    private Integer id;

    private Byte inusing;

    private String apinum;

    private String apipwd;

    private String isban;
    
    private String token;

    private Date createtime;
    
    private Date tokentime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Byte getInusing() {
        return inusing;
    }

    public void setInusing(Byte inusing) {
        this.inusing = inusing;
    }

    public String getApinum() {
        return apinum;
    }

    public void setApinum(String apinum) {
        this.apinum = apinum == null ? null : apinum.trim();
    }

    public String getApipwd() {
        return apipwd;
    }

    public void setApipwd(String apipwd) {
        this.apipwd = apipwd == null ? null : apipwd.trim();
    }

    public String getIsban() {
        return isban;
    }

    public void setIsban(String isban) {
        this.isban = isban == null ? null : isban.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getTokentime() {
		return tokentime;
	}

	public void setTokentime(Date tokentime) {
		this.tokentime = tokentime;
	}
}