package com.easy.security;

import java.util.Collection;

import javax.annotation.Resource;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.easy.entity.Owner;
import com.easy.entity.User;
import com.easy.service.ManagerService;
import com.easy.service.UserService;

public class UserRealm extends AuthorizingRealm {

	@Resource
	private UserService userService;
	@Resource
	private ManagerService managerService;
	@Autowired
	private SessionDAO sessionDAO;

	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		SimpleAuthorizationInfo authorizationInfo = null;
		try {
			String loginname = (String) principals.getPrimaryPrincipal();
			Owner owner = managerService.getOwnerByUsername(loginname);
			authorizationInfo = new SimpleAuthorizationInfo();
			authorizationInfo.addRole(owner.getRolename());
		} catch (Exception e) {
		}
		return authorizationInfo;
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		String loginname = (String) token.getPrincipal();
		AuthenticationInfo authcInfo;
		if (loginname.startsWith("manager-")) {
			Owner owner = managerService.getOwnerByUsername(loginname.split("-")[1]);
			if (owner != null) {
				authcInfo = new SimpleAuthenticationInfo(owner.getUsername(), owner.getPassword(), "userRealm");
				return authcInfo;
			} else {
				return null;
			}
		} else {
			// 限制一处登陆
			Collection<Session> sessions = sessionDAO.getActiveSessions();
			// 循环当前在线的人数，加入登录控制，避免账号重复登录
			for (Session session : sessions) {
				// 获得session中已经登录用户的名字
				String loginUsername = String
						.valueOf(session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY));
				if (loginname.equals(loginUsername)) { // 判断当前用户是否已经登录
					session.setTimeout(0); // 如果登录就挤掉之前的登录
					break;
				}
			}

			User user = userService.getUserByMobile(loginname);
			if (user != null) {
				authcInfo = new SimpleAuthenticationInfo(user.getMobile(), user.getPassword(), "userRealm");
				return authcInfo;

			} else {
				return null;
			}
		}
	}
}
