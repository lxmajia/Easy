/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50527
Source Host           : 127.0.0.1:3306
Source Database       : jianzhi

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2017-09-11 20:25:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bills
-- ----------------------------
DROP TABLE IF EXISTS `bills`;
CREATE TABLE `bills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `detailid` int(11) DEFAULT NULL,
  `serialid` int(11) DEFAULT NULL,
  `security` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `smscode` varchar(255) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `applytime` datetime DEFAULT NULL,
  `overtime` datetime DEFAULT NULL,
  `suretime` datetime DEFAULT NULL,
  `money` varchar(255) DEFAULT NULL,
  `billtype` varchar(255) DEFAULT NULL,
  `billby` int(255) DEFAULT NULL,
  `billtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bills
-- ----------------------------
INSERT INTO `bills` VALUES ('1', '2', '1', '1', '2d446944-953c-4a3a-ba19-cd0f398548cc', '15680747336', '963121', '30', '2017-09-10 15:51:56', '2017-09-10 16:09:13', '2017-09-10 17:11:01', '500', 'zhifubao', '1', '2017-09-10 18:23:08');

-- ----------------------------
-- Table structure for detail
-- ----------------------------
DROP TABLE IF EXISTS `detail`;
CREATE TABLE `detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serialid` int(11) DEFAULT NULL,
  `security` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `getphone` tinyint(4) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `smscode` varchar(255) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `checkflag` int(4) DEFAULT NULL,
  `applytime` datetime DEFAULT NULL,
  `overtime` datetime DEFAULT NULL,
  `suretime` datetime DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of detail
-- ----------------------------
INSERT INTO `detail` VALUES ('12', '1', '1', '2d446944-953c-4a3a-ba19-cd0f398548cc', '1', '15680747336', '1', '963121', '30', '0', '2017-09-11 20:20:43', null, null, null);

-- ----------------------------
-- Table structure for owner
-- ----------------------------
DROP TABLE IF EXISTS `owner`;
CREATE TABLE `owner` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `lasttime` datetime DEFAULT NULL,
  `rolename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of owner
-- ----------------------------
INSERT INTO `owner` VALUES ('1', 'admin', '3dd0caee3b657fe458fc64e967b1cd94', '2017-09-07 20:47:31', '2017-09-07 21:05:33', 'admin');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `rolename` varchar(255) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------

-- ----------------------------
-- Table structure for servernum
-- ----------------------------
DROP TABLE IF EXISTS `servernum`;
CREATE TABLE `servernum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inusing` tinyint(255) DEFAULT NULL,
  `apinum` varchar(255) DEFAULT NULL,
  `apipwd` varchar(255) DEFAULT NULL,
  `isban` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `tokentime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of servernum
-- ----------------------------
INSERT INTO `servernum` VALUES ('1', '8', 'api-434ewsq9', 'tz112233', '0', '2d446944-953c-4a3a-ba19-cd0f398548cc', '2017-09-04 20:18:18', '2017-09-10 20:17:36');
INSERT INTO `servernum` VALUES ('2', '0', 'thisisnum', 'thisispwd', '1', 'nosuccessfortoken', '2017-09-10 20:12:37', '2017-09-10 20:34:24');

-- ----------------------------
-- Table structure for sysstate
-- ----------------------------
DROP TABLE IF EXISTS `sysstate`;
CREATE TABLE `sysstate` (
  `id` int(11) NOT NULL,
  `sysinfo` varchar(255) DEFAULT NULL,
  `working` varchar(255) DEFAULT NULL,
  `changetime` datetime DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `changeby` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sysstate
-- ----------------------------
INSERT INTO `sysstate` VALUES ('1', 'mysoft', '1', '2017-09-08 20:34:37', '', '1');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) DEFAULT NULL,
  `mobile` varchar(32) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `allowcount` int(11) DEFAULT NULL,
  `jointime` datetime DEFAULT NULL,
  `lasttime` datetime DEFAULT NULL,
  `banflag` tinyint(4) DEFAULT NULL,
  `zhifubao` varchar(255) DEFAULT NULL,
  `weixin` varchar(255) DEFAULT NULL,
  `rolename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('30', 'LLL丶禾羊', '15680747337', '3dd0caee3b657fe458fc64e967b1cd94', '5', '2017-09-09 18:59:22', null, '0', '15680747337', '15680747337', 'user');
INSERT INTO `user` VALUES ('31', 'liaoxiang', '1546804155', '7bd156430f08da9e21bf302dc7989718', '5', '2017-09-10 16:54:43', null, '0', '15680747777', '777777', 'user');
