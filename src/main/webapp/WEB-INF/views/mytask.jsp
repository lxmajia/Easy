<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>我的任务</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/res/css/layui.css">
<Link Rel="SHORTCUT ICON" href="${pageContext.request.contextPath}/res/favicon.ico">
</head>
<body>


	<div style="text-align: center;">
		<a style="right: 0px;"
			href="${pageContext.request.contextPath}/index.htm"><span
			class="layui-badge layui-bg-blue">返回主页</span></a>
	</div>
	<div style="text-align: center;">
		<h4>你今天已经申请了${detaillist.size() }次</h4>
	</div>

	<div>
		<table class="layui-table">
			<colgroup>
				<col width="10%">
				<col width="30%">
				<col width="30%">
				<col width="30%">
			</colgroup>
			<thead>
				<tr>
					<th>No</th>
					<th>Phone</th>
					<th>Code</th>
					<th>State</th>
				</tr>
			</thead>
			<tbody>

				<c:forEach items="${detaillist }" var="d" varStatus="count">
					<tr>
						<td>${count.index+1 }</td>
						<c:if test="${empty d.phone }">
							<td>待获取手机号</td>
						</c:if>
						<c:if test="${!empty d.phone }">
							<td>${d.phone }</td>
						</c:if>

						<c:if test="${empty d.smscode }">
							<td>待获取验证码</td>
						</c:if>
						<c:if test="${!empty d.smscode }">
							<td>${d.smscode }</td>
						</c:if>
						
						<c:if test="${empty d.checkflag }">
							<td><span class="layui-badge">还未开始</span></td>
						</c:if>
						<c:if test="${d.checkflag == 1 }">
							<td><span class="layui-badge layui-bg-green">等待管理员审核</span></td>
						</c:if>
						<c:if test="${d.checkflag == 2 }">
							<c:if test="${d.content =='success' }">
								<td><span class="layui-badge layui-bg-blue">审核通过</span></td>
							</c:if>
							<c:if test="${d.content =='failed' }">
								<td style="color:#00FF00;">失败(管理员审核未通过)</td>
							</c:if>
							<c:if test="${d.content =='havado' }">
								<td style="color:#FFFF00;">失败(用户提交已绑定)</td>
							</c:if>
							<c:if test="${d.content =='ban' }">
								<td style="color:#FF0000;">频繁(用户提交太频繁)</td>
							</c:if>
						</c:if>
						<c:if test="${d.checkflag == 3 }">
							<td>审核失败</td>
						</c:if>
						<c:if test="${d.checkflag == 4 }">
							<td><span class="layui-badge layui-bg-blue">已经结算</span></td>
						</c:if>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	
	<p style="color: red;">此系统主要解决学生平常时间比较少！可以闲暇时间操作</p>
	<p style="color: red;">打款时间（20:00-00:00）</p>

	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/layui.all.js"></script>
</body>
</html>