<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>创建新任务</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/res/css/layui.css">
<Link Rel="SHORTCUT ICON" href="${pageContext.request.contextPath}/res/favicon.ico">
</head>
<body style="text-align: center;">
	<div style="text-align: center;">
		<a style="right: 0px;"
			href="${pageContext.request.contextPath}/index.htm"><span
			class="layui-badge layui-bg-blue">返回主页</span></a>
	</div>

	<div>
		<fieldset class="layui-elem-field layui-field-title">
			<div class="layui-field-box">当前任务为空！请新建！</div>
		</fieldset>
	</div>

	<a style="width: 60%;" class="layui-btn layui-btn-normal" href="${pageContext.request.contextPath}/detail/createtask.htm">点击新建</a>
	<br>
	<br>
	<p style="color: red;">请每一步，严格按照操作来进行！否则会出现错误！重要的事强调3次！</p>
	<p style="color: red;">
		请勿把新号拿来在这里测试！发现将会<b>永久拉黑</b>！
	</p>
	<p style="color: red;">
		请勿把新号拿来在这里测试！发现将会<b>永久拉黑</b>！
	</p>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/layui.all.js"></script>
</body>
</html>