<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理员数据平台--结算中心</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/res/css/layui.css">
<Link Rel="SHORTCUT ICON" href="${pageContext.request.contextPath}/res/favicon.ico">
</head>
<body>
	<div style="text-align: center;">
		<a style="right: 0px;"
			href="${pageContext.request.contextPath}/manager/index.htm"><span
			class="layui-badge layui-bg-blue">返回主页</span></a>
		<p>${msg }</p>
	</div>
	<blockquote class="layui-elem-quote">
		用户<span class="layui-badge-rim">${user.mobile }</span>可结算次数：<span
			class="layui-badge-rim">${details.size() }</span>
	</blockquote>
	<div>
		<div>
			<table class="layui-table">
				<colgroup>
					<col width="20%">
					<col width="20%">
					<col width="20%">
				</colgroup>
				<thead>
					<th>手机号</th>
					<th>验证码</th>
					<th>完成时间</th>
					<th>审核时间</th>
					<th>审核状态</th>
				</thead>
				<tbody>
					<c:forEach items="${details }" var="d">
						<tr>
							<td>${d.phone }</td>
							<td>${d.smscode }</td>
							<td><fmt:formatDate type="both" value="${d.overtime }" /></td>
							<td><fmt:formatDate type="both" value="${d.suretime }" /></td>
							<c:if test="${d.checkflag ==2 }">
								<td><span class="layui-badge layui-bg-blue">通过</span></td>
							</c:if>
							<c:if test="${d.checkflag ==3 }">
								<td><span class="layui-badge layui-bg-blue">失败</span></td>
							</c:if>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<hr>
	<div style="text-align: center;width:300px;margin:0 auto;">
		<form class="layui-form" action="${pageContext.request.contextPath}/manager/givemoney.htm" method="post">
			<input type="hidden" name="uid" value="${user.id }">
			<div class="layui-form-item">
				<label class="layui-form-label">支付方式</label>
				<div class="layui-input-block">
					<select name="billtype" lay-verify="required">
						<option value="zhifubao">支付宝</option>
						<option value="weixin">微信</option>
					</select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">支付金额</label>
				<div class="layui-input-block">
					<input type="text" name="money" required lay-verify="required"
						placeholder="￥" autocomplete="off" class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<button class="layui-btn" lay-submit lay-filter="formDemo">支付成功</button>
					<button type="reset" class="layui-btn layui-btn-primary">重置</button>
				</div>
			</div>
		</form>
	</div>
	<hr>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/layui.all.js"></script>
</body>
</html>