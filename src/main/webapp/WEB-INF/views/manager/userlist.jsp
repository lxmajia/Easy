<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理员数据平台--用户列表</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/res/css/layui.css">
<Link Rel="SHORTCUT ICON" href="${pageContext.request.contextPath}/res/favicon.ico">
</head>
<body>
	<div style="text-align: center;">
		<a style="right: 0px;"
			href="${pageContext.request.contextPath}/manager/index.htm"><span
			class="layui-badge layui-bg-blue">返回主页</span></a> | <a
			style="right: 0px;"
			href="${pageContext.request.contextPath}/manager/adduserpage.htm"><span
			class="layui-badge layui-bg-blue">添加用户</span></a>
		<p>${msg }</p>
	</div>
	<blockquote class="layui-elem-quote">用户管理中心</blockquote>

	<div style="text-align: center; margin: 0 auto;">
		<form action="#" method="post">
			<input style="width: 300px; margin: 0 auto;" type="text" name="mobile"
				value="${mobile }" placeholder="请输入手机号" class="layui-input">
			<button lay-submit  class="layui-btn layui-btn-normal">过滤搜索</button>
		</form>
	</div>
	<div>
		<div>
			<table class="layui-table">
				<colgroup>
					<col width="13%">
					<col width="12%">
					<col width="10%">
					<col width="20%">
					<col width="15%">
					<col width="15%">
				</colgroup>
				<thead>
					<tr>
						<th>用户名</th>
						<th>手机号</th>
						<th>次/天</th>
						<th>加入时间</th>
						<th>支付宝</th>
						<th>微信</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${users }" var="u">
						<tr>
							<td>${u.username }</td>
							<td>${u.mobile }</td>
							<td><red style="color:red;">${u.allowcount }</red>次/天</td>
							<td><fmt:formatDate type="both" value="${u.jointime }" /></td>
							<td>${u.zhifubao }</td>
							<td>${u.weixin }</td>
							<c:if test="${u.banflag == 0 }">
								<td><a
									href="${pageContext.request.contextPath}/manager/locked.htm?uid=${u.id }&pagenow=${page.startPage }">禁用</a>&nbsp;|&nbsp;<a
									href="${pageContext.request.contextPath}/manager/resetpwd.htm?uid=${u.id }&pagenow=${page.startPage }">重置密码</a>&nbsp;|&nbsp;<a
									href="${pageContext.request.contextPath}/manager/showdetails.htm?uid=${u.id }">今日任务</a>&nbsp;|&nbsp;<a
									href="${pageContext.request.contextPath}/manager/givemoneypage.htm?uid=${u.id }">结算</a>&nbsp;|&nbsp;<a
									href="${pageContext.request.contextPath}/manager/changeuserpage.htm?uid=${u.id }">修改信息</a>&nbsp;|&nbsp;<a
									href="${pageContext.request.contextPath}/manager/deleteuser.htm?uid=${u.id }&pagenow=${page.startPage }">删除</a>
							</c:if>
							<c:if test="${u.banflag == 1 }">
								<td><a
									href="${pageContext.request.contextPath}/manager/unlock.htm?uid=${u.id }&pagenow=${page.startPage }"
									style="color: red;">解封</a></td>
							</c:if>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>

		<div>
			当前是第${page.startPage }页/共${page.totalPageCount }页</br>
			<c:choose>
				<c:when test="${page.totalPageCount!=0 && page.startPage!=1}">
					<a
						href="${pageContext.request.contextPath}/manager/user.htm?pagenow=1">首页</a>
				</c:when>
			</c:choose>
			<c:choose>
				<c:when test="${page.startPage - 1 > 0}">
					<a
						href="${pageContext.request.contextPath}/manager/user.htm?pagenow=${page.startPage - 1}">上一页</a>
				</c:when>
			</c:choose>
			<c:choose>
				<c:when test="${page.startPage + 1 <= page.totalPageCount}">
					<a
						href="${pageContext.request.contextPath}/manager/user.htm?pagenow=${page.startPage + 1}">下一页</a>
				</c:when>
			</c:choose>
			<c:choose>
				<c:when
					test="${page.totalPageCount!=0 && page.totalPageCount!=page.startPage}">
					<a
						href="${pageContext.request.contextPath}/manager/user.htm?pagenow=${page.totalPageCount}">尾页</a>
				</c:when>
			</c:choose>
		</div>
	</div>

	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/layui.all.js"></script>
	<script>
		//注意：导航 依赖 element 模块，否则无法进行功能性操作
		layui.use('element', function() {
			var element = layui.element;

			//…
		});
	</script>
</body>
</html>