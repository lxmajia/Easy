<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理员数据平台--账号列表</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/res/css/layui.css">
<Link Rel="SHORTCUT ICON" href="${pageContext.request.contextPath}/res/favicon.ico">
</head>
<body>
	<div style="text-align: center;">
		<a style="right: 0px;"
			href="${pageContext.request.contextPath}/manager/index.htm"><span
			class="layui-badge layui-bg-blue">返回主页</span></a>
	</div>
	<blockquote class="layui-elem-quote">结算中心</blockquote>
	<div>
		<div>
			<table class="layui-table">
				<colgroup>
					<col width="10%">
					<col width="10%">
					<col width="25%">
					<col width="15%">
					<col width="15%">
					<col width="10%">
					<col width="15%">
				</colgroup>
				<thead>
					<th>Api账号</th>
					<th>Api密码</th>
					<th>Token</th>
					<th>添加时间</th>
					<th>token时间</th>
					<th>状态</th>
					<th>操作</th>
				</thead>
				<tbody>
					<c:forEach items="${serverlist }" var="s">
						<tr>
							<td>${s.apinum }</td>
							<td>${s.apipwd }</td>
							<td>${s.token }</td>
							<td><fmt:formatDate type="both" value="${s.createtime }" /></td>
							<td><fmt:formatDate type="both" value="${s.tokentime }" /></td>
							<c:if test="${s.isban == '0' }">
								<td><span class="layui-badge layui-bg-blue">已启动</span></td>
							</c:if>
							<c:if test="${s.isban == '1' }">
								<td><span class="layui-badge layui-bg-black">未启动</span></td>
							</c:if>
							<c:if test="${s.isban == '0' }">
								<td><a
									href="${pageContext.request.contextPath}/manager/changeserverstate.htm?sid=${s.id }&state=1">禁用</a>|<a
									href="${pageContext.request.contextPath}/manager/deleteservernum.htm?sid=${s.id }">删除</a></td>
							</c:if>
							<c:if test="${s.isban == '1' }">
								<td><a
									href="${pageContext.request.contextPath}/manager/changeserverstate.htm?sid=${s.id }&state=0">启用</a>|<a
									href="${pageContext.request.contextPath}/manager/deleteservernum.htm?sid=${s.id }">删除</a></td>
							</c:if>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<div>
			<p style="color: red; text-align: center;">提示：如果在添加过程中选择启用或者操作中选择启用不能启用成功的，表明此号在获取token时出现错误！请确保账号正确！</p>
		</div>
		<div style="width: 400px; margin: 0 auto;">
			<form
				action="${pageContext.request.contextPath}/manager/addservernum.htm"
				method="post">
				<div class="layui-form">
					<div class="layui-form-item">
						<label class="layui-form-label">Api账号</label>
						<div class="layui-input-block">
							<input type="text" name="apinum" required lay-verify="required"
								placeholder="Api账号" autocomplete="off" class="layui-input">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">Api密码</label>
						<div class="layui-input-block">
							<input type="text" name="apipwd" required lay-verify="required"
								placeholder="Api密码" autocomplete="off" class="layui-input">
						</div>
					</div>

					<div class="layui-form-item">
						<label class="layui-form-label">状态</label>
						<div class="layui-input-block">
							<select name="isban" lay-verify="required">
								<option value="0">启用</option>
								<option value="1">禁用</option>
							</select>
						</div>
					</div>
					<div class="layui-form-item">
						<div class="layui-input-block">
							<button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/layui.all.js"></script>
</body>
</html>