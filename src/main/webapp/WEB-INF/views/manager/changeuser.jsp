<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>兼职人员登录页面--用户信息更改</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/res/css/layui.css">
<Link Rel="SHORTCUT ICON" href="${pageContext.request.contextPath}/res/favicon.ico">
</head>
<body>

	<br>
	<div style="text-align: center;">
		<button class="layui-btn layui-btn-disabled">欢迎使用便捷操作平台助手工具V1.0</button>
	</div>
	<div style="text-align: center;">
		<a style="right: 0px;"
			href="${pageContext.request.contextPath}/manager/index.htm"><span
			class="layui-badge layui-bg-blue">返回主页</span></a>
	</div>
	<br>
	<div style="width: 400px; margin: 0 auto;">
		<form action="${pageContext.request.contextPath}/manager/saveuser.htm"
			method="post">
			<input name="id" value="${user.id }" type="hidden">
			<div class="layui-form-item">
				<label class="layui-form-label">手机号</label>
				<div class="layui-input-block">
					<input style="width: 190px;" type="text" name="mobile" required
						lay-verify="required" readonly="readonly" value="${user.mobile }" autocomplete="off"
						class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">次数/天</label>
				<div class="layui-input-block">
					<input style="width: 190px;" type="number" min="0" max="100" name="allowcount" required
						lay-verify="required" placeholder="请输入此管理员新密码" value="${user.allowcount }" autocomplete="off"
						class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">支付宝</label>
				<div class="layui-input-block">
					<input style="width: 190px;" type="text" name="zhifubao" required
						lay-verify="required" placeholder="请输入新支付宝" autocomplete="off"
						class="layui-input" value="${user.zhifubao }">
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<button class="layui-btn" lay-submit lay-filter="formDemo">立即更改</button>
				</div>
			</div>
		</form>

	</div>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/layui.all.js"></script>
</body>
</html>