<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理员数据平台-登录</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/res/css/layui.css">
<Link Rel="SHORTCUT ICON" href="${pageContext.request.contextPath}/res/favicon.ico">
</head>
<body>

	<br>
	<br>
	<div style="text-align: center;">
	<button class="layui-btn layui-btn-disabled">欢迎使用便捷操作平台助手工具V1.0</button><br>
	<button class="layui-btn layui-btn-disabled">管理员登录入口</button>
	</div>
	<br>
	<br>
	<div style="width: 400px; margin: 0 auto;">
		<p style="color: red;">${msg }</p>
		<form action="${pageContext.request.contextPath}/manager/login.htm"
			method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">用户名</label>
				<div class="layui-input-block">
					<input style="width: 190px;" type="text" name="username" required
						lay-verify="required" placeholder="请输入标题" autocomplete="off"
						class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">密码框</label>
				<div class="layui-input-inline">
					<input type="password" name="password" required
						lay-verify="required" placeholder="请输入密码" autocomplete="off"
						class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
					<button type="reset" class="layui-btn layui-btn-primary">重置</button>
				</div>
			</div>
		</form>
	</div>
	<br>
	<br>
	<br>
	<div style="width: 100px; margin: 0 auto;">
		<a class="layui-btn layui-btn-normal" target="_self"
			href="${pageContext.request.contextPath}/login.htm">我是工作人员</a>
	</div>
	
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/layui.all.js"></script>
</body>
</html>