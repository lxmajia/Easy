<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理员数据平台--验收</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/res/css/layui.css">
<Link Rel="SHORTCUT ICON" href="${pageContext.request.contextPath}/res/favicon.ico">
</head>
<body>
	<div style="text-align: center;">
		<a style="right: 0px;"
			href="${pageContext.request.contextPath}/manager/index.htm"><span
			class="layui-badge layui-bg-blue">返回主页</span></a>
		<p>${msg }</p>
	</div>
	<blockquote class="layui-elem-quote">业务审核中心</blockquote>
	<div>
		<div>
			<table class="layui-table">
				<colgroup>
					<col width="20%">
					<col width="20%">
					<col width="30%">
					<col width="20%">
				</colgroup>
				<thead>
					<th>手机号</th>
					<th>验证码</th>
					<th>Token</th>
					<th>完成时间</th>
					<th>操作</th>
				</thead>
				<tbody>
					<c:forEach items="${details }" var="d">
						<tr>
							<td>${d.phone }</td>
							<td>${d.smscode }</td>
							<td>"xuebian"+${d.token }+"bianxue"</td>
							<td><fmt:formatDate type="both" value="${d.overtime }" /></td>
							<td><a
								href="${pageContext.request.contextPath}/manager/tasksuccess.htm?did=${d.id }">通过</a>&nbsp;|&nbsp;<a
								href="${pageContext.request.contextPath}/manager/taskfailed.htm?did=${d.id }">失败</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>

		<div>
			当前是第${page.startPage }页/共${page.totalPageCount }页</br>
			<c:choose>
				<c:when test="${page.totalPageCount!=0 && page.startPage!=1}">
					<a
						href="${pageContext.request.contextPath}/manager/detailcheck.htm?pagenow=1">首页</a>
				</c:when>
			</c:choose>
			<c:choose>
				<c:when test="${page.startPage - 1 > 0}">
					<a
						href="${pageContext.request.contextPath}/manager/detailcheck.htm?pagenow=${page.startPage - 1}">上一页</a>
				</c:when>
			</c:choose>
			<c:choose>
				<c:when test="${page.startPage + 1 <= page.totalPageCount}">
					<a
						href="${pageContext.request.contextPath}/manager/detailcheck.htm?pagenow=${page.startPage + 1}">下一页</a>
				</c:when>
			</c:choose>
			<c:choose>
				<c:when
					test="${page.totalPageCount!=0 && page.totalPageCount!=page.startPage}">
					<a
						href="${pageContext.request.contextPath}/manager/detailcheck.htm?pagenow=${page.totalPageCount}">尾页</a>
				</c:when>
			</c:choose>
		</div>
	</div>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/layui.all.js"></script>
</body>
</html>