<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理员数据平台--用户添加</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/res/css/layui.css">
<Link Rel="SHORTCUT ICON" href="${pageContext.request.contextPath}/res/favicon.ico">
</head>
<body>
	<div style="text-align: center;">
		<a style="right: 0px;"
			href="${pageContext.request.contextPath}/manager/index.htm"><span
			class="layui-badge layui-bg-blue">返回主页</span></a> | <a
			style="right: 0px;"
			href="${pageContext.request.contextPath}/manager/user.htm"><span
			class="layui-badge layui-bg-blue">用户列表</span></a>
	</div>
	<blockquote class="layui-elem-quote">用户添加中心</blockquote>
	<div style="width: 400px; margin: 0 auto;text-align: center;">
		<p style="color:red;">${msg }</p>
	</div>
	<div style="width: 400px; margin: 0 auto;">
		<form action="${pageContext.request.contextPath}/manager/adduser.htm"
			method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">用户名</label>
				<div class="layui-input-block">
					<input style="width: 190px;" type="text" name="username" required
						lay-verify="required" placeholder="请输入用户名" autocomplete="off"
						class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">手机号</label>
				<div class="layui-input-block">
					<input style="width: 190px;" type="text" name="mobile" required
						lay-verify="required" placeholder="手机号（登录用）" autocomplete="off"
						class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">任务数</label>
				<div class="layui-input-block">
					<input style="width: 190px;" type="number" min="0" max="100" name="allowcount" required
						lay-verify="required" placeholder="任务量" autocomplete="off"
						class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">支付宝</label>
				<div class="layui-input-block">
					<input style="width: 190px;" type="text" name="zhifubao" required
						lay-verify="required" placeholder="请输入收款支付宝（推荐）" autocomplete="off"
						class="layui-input">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">微信</label>
				<div class="layui-input-block">
					<input style="width: 190px;" type="text" name="weixin" required
						lay-verify="required" placeholder="请输入微信" autocomplete="off"
						class="layui-input">
				</div>
			</div>
			
			
			<div class="layui-form-item">
				<div class="layui-input-block">
					<button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
					<button type="reset" class="layui-btn layui-btn-primary">重置</button>
				</div>
			</div>
		</form>
	</div>
	
	
	
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/layui.all.js"></script>
</body>
</html>