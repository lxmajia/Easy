<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理员数据平台--系统设置</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/res/css/layui.css">
<Link Rel="SHORTCUT ICON" href="${pageContext.request.contextPath}/res/favicon.ico">
</head>
<body>
	<div style="text-align: center;">
		<a style="right: 0px;"
			href="${pageContext.request.contextPath}/manager/index.htm"><span
			class="layui-badge layui-bg-blue">返回主页</span></a>
	</div>
	<blockquote class="layui-elem-quote">系统状态设置</blockquote>

	<div style="text-align: center;margin: 0 auto;">
		<form action="${pageContext.request.contextPath}/manager/savesys.htm"
			method="post">
			<input type="hidden" name="id" value="${sys.id }"> <input
				type="hidden" name="sysinfo" value="${sys.sysinfo }">
			<div class="layui-form-item">
				<div>
					<select name="working" lay-verify="required" style="width: 300px;height: 46px;font-size: 36px;">
						<c:if test="${sys.working =='1' }">
							<option value="1" selected="selected">开启</option>
							<option value="0">结算中</option>
							<option value="-1">禁止登录</option>
						</c:if>
						<c:if test="${sys.working =='0' }">
							<option value="1">开启</option>
							<option value="0" selected="selected">结算中</option>
							<option value="-1">禁止登录</option>
						</c:if>
						<c:if test="${sys.working =='-1' }">
							<option value="1">开启</option>
							<option value="0">结算中</option>
							<option value="-1" selected="selected">禁止登录</option>
						</c:if>
					</select>
				</div>
			</div>

			<div style="text-align: center;margin: 0 auto;">
				<div>
					<input name="content" style="width: 300px;height: 24px;" placeholder="请输入公告！">
				</div>
			</div>
			<br><br><br>
			<input class="layui-btn" type="submit" lay-submit value="保存">

		</form>
	</div>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/layui.all.js"></script>
	<script>
		//注意：导航 依赖 element 模块，否则无法进行功能性操作
		layui.use('element', function() {
			var element = layui.element;

			//…
		});
	</script>
</body>
</html>