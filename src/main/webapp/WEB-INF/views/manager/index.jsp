<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理员数据平台</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/res/css/layui.css">
<Link Rel="SHORTCUT ICON" href="${pageContext.request.contextPath}/res/favicon.ico">
</head>
<body>
	<shiro:hasRole name="admin">
		<br>
		<div style="text-align: center;">
			<button style="width: 260px;" class="layui-btn layui-btn-disabled">欢迎你:${user.username }</button>
		</div>

		<blockquote class="layui-elem-quote">
			今日用户提交成功次数：<span class="layui-badge layui-bg-blue">${success }</span>
		</blockquote>
		<blockquote class="layui-elem-quote">
			今日出现已被绑定次数：<span class="layui-badge layui-bg-orange">${failed }</span>
		</blockquote>
		<blockquote class="layui-elem-quote">
			今日出现操作频繁次数：<span class="layui-badge">${ban }</span>
		</blockquote>
	</shiro:hasRole>

	<shiro:hasRole name="admin">
		<div style="text-align: center;">
			<a style="width: 260px;" class="layui-btn"
				href="${pageContext.request.contextPath}/manager/user.htm">人员管理</a><br>
			<br> <a style="width: 260px;" class="layui-btn layui-btn-normal"
				href="${pageContext.request.contextPath}/manager/detail.htm">数据浏览</a><br>
			<br> <a style="width: 260px;" class="layui-btn layui-btn-warm"
				href="${pageContext.request.contextPath}/manager/detailcheck.htm">业务处理</a><br>

			<br> <a style="width: 260px;" class="layui-btn layui-btn-danger"
				href="${pageContext.request.contextPath}/manager/summoney.htm">提前结算</a>
			<br> <br> <a style="width: 260px;"
				class="layui-btn layui-btn-danger"
				href="${pageContext.request.contextPath}/manager/servermunlist.htm">API账号</a>
			<br> <br> <a style="width: 260px;"
				class="layui-btn layui-btn-danger"
				href="${pageContext.request.contextPath}/manager/sysset.htm">系统设置</a>
			<br> <br> <a style="width: 260px;"
				class="layui-btn layui-btn-danger"
				href="${pageContext.request.contextPath}/manager/pwdpage.htm">更改密码</a><br>
			<br> <a style="width: 260px;" class="layui-btn layui-btn-danger"
				href="${pageContext.request.contextPath}/logout.htm">安全退出</a><br>
			<br>
		</div>
	</shiro:hasRole>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/layui.all.js"></script>
</body>
</html>