<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理员数据平台--列表</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/res/css/layui.css">
<Link Rel="SHORTCUT ICON" href="${pageContext.request.contextPath}/res/favicon.ico">
</head>
<body>
	<div style="text-align: center;">
		<a style="right: 0px;"
			href="${pageContext.request.contextPath}/manager/index.htm"><span
			class="layui-badge layui-bg-blue">返回主页</span></a>
		<p>${msg }</p>
	</div>
	<blockquote class="layui-elem-quote">业务详情中心</blockquote>
	<div style="text-align: center; margin: 0 auto;">
		<form action="${pageContext.request.contextPath}/manager/detail.htm" method="post">
			<input style="width: 300px; margin: 0 auto;" type="text" name="mobile"
				value="${mobile }" placeholder="请输入获取的手机号" class="layui-input">
			<button lay-submit  class="layui-btn layui-btn-normal">搜索</button>
		</form>
	</div>
	
	<div>
		<div>
			<table class="layui-table">
				<colgroup>
					<col width="5%">
					<col width="13%">
					<col width="12%">
					<col width="10%">
					<col width="20%">
					<col width="15%">
				</colgroup>
				<thead>
					<th>用户ID</th>
					<th>手机号</th>
					<th>验证码</th>
					<th>完成时间</th>
					<th>Token</th>
					<th>状态</th>
					<th>操作</th>
				</thead>
				<tbody>
					<c:forEach items="${details }" var="d">
						<tr>
							<td>${d.userid }</td>
							<td>${d.phone }</td>
							<td>${d.smscode }</td>
							<td><fmt:formatDate type="both" value="${d.overtime }" /></td>
							<td>${d.token }</td>

							<c:if test="${empty d.checkflag }">
								<td><span class="layui-badge">还未开始</span></td>
							</c:if>
							<c:if test="${d.checkflag == 1 }">
								<td><span class="layui-badge layui-bg-green">还未完成</span></td>
							</c:if>
							<c:if test="${d.checkflag == 2 }">
								<c:if test="${d.content =='success' }">
									<td><span class="layui-badge layui-bg-blue">审核通过</span></td>
								</c:if>
								<c:if test="${d.content =='failed' }">
									<td style="color: #00FF00;">失败(管理员审核未通过)</td>
								</c:if>
								<c:if test="${d.content =='havado' }">
									<td style="color: #FFFF00;">失败(用户提交已绑定)</td>
								</c:if>
								<c:if test="${d.content =='ban' }">
									<td style="color: #FF0000;">频繁(用户提交太频繁)</td>
								</c:if>
							</c:if>
							<c:if test="${d.checkflag == 3 }">
								<td>审核失败</td>
							</c:if>
							<c:if test="${d.checkflag == 4 }">
								<td><span class="layui-badge layui-bg-blue">已经结算</span></td>
							</c:if>

							<td><a
								href="${pageContext.request.contextPath}/manager/changesuccess.htm?did=${d.id }&pagenow=${page.startPage }">纠正(通过)</a>&nbsp;|&nbsp;
								<a
								href="${pageContext.request.contextPath}/manager/deletedetail.htm?did=${d.id }&pagenow=${page.startPage }">删除</a>

							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>

		<div>
			当前是第${page.startPage }页/共${page.totalPageCount+1 }页</br>
			<c:choose>
				<c:when test="${page.totalPageCount!=0 && page.startPage!=1}">
					<a
						href="${pageContext.request.contextPath}/manager/detail.htm?pagenow=1">首页</a>
				</c:when>
			</c:choose>
			<c:choose>
				<c:when test="${page.startPage - 1 > 0}">
					<a
						href="${pageContext.request.contextPath}/manager/detail.htm?pagenow=${page.startPage - 1}">上一页</a>
				</c:when>
			</c:choose>
			<c:choose>
				<c:when test="${page.startPage + 1 <= page.totalPageCount}">
					<a
						href="${pageContext.request.contextPath}/manager/detail.htm?pagenow=${page.startPage + 1}">下一页</a>
				</c:when>
			</c:choose>
			<c:choose>
				<c:when
					test="${page.totalPageCount!=0 && page.totalPageCount!=page.startPage}">
					<a
						href="${pageContext.request.contextPath}/manager/detail.htm?pagenow=${page.totalPageCount}">尾页</a>
				</c:when>
			</c:choose>
		</div>
		<br> <br> <br> <br> <br> <br> <br>
		<br> <br> <br> <br> <br>
		<shiro:hasRole name="admin">
			<div style="text-align: center;">
				<a style="width: 260px;" class="layui-btn" onclick="cleardata()"
					href="${pageContext.request.contextPath}/manager/flushdetail.htm">清空数据</a><br>
			</div>
		</shiro:hasRole>
	</div>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/layui.all.js"></script>



</body>
</html>