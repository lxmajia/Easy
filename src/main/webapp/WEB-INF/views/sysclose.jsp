<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<Link Rel="SHORTCUT ICON" href="${pageContext.request.contextPath}/res/favicon.ico">
<title>系统关闭提醒</title>
</head>
<body>
	<div style="text-align: center;">
		<h1>关闭通知</h1>
		<br>
		<br>
			<h3 style="color:red;">管理员还未开放系统，请稍后再试</h3>
			<h2>公告</h2>
			<p>${content }</p>
		<br>
		<br>
		<br> <a target="_self"
			href="${pageContext.request.contextPath}/managerlogin.htm">我是管理员</a>
	</div>
</body>
</html>