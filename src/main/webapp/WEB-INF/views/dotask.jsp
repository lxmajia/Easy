<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/res/css/layui.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/res/css/layer.css">
<Link Rel="SHORTCUT ICON" href="${pageContext.request.contextPath}/res/favicon.ico">
<title>继续做任务</title>
</head>
<body>
	<input type="hidden" id="ctx"
		value="${pageContext.request.contextPath}">
	<div style="text-align: center;">
		<a style="right: 0px;"
			href="${pageContext.request.contextPath}/index.htm"><span
			class="layui-badge layui-bg-blue">返回主页</span></a>
	</div>
	<div>
		<blockquote class="layui-elem-quote">
			这是今天的第<span class="layui-badge-rim">${detail.serialid }</span>个任务了！
		</blockquote>
	</div>
	<c:if test="${empty detail }">
		<a
			href="${pageContext.request.contextPath}/api/getPhone.htm?id=${detail.id }">新建</a>
	</c:if>
	<c:if test="${empty detail.phone }">
		<div style="text-align: center;">
			<a class="layui-btn"
				href="${pageContext.request.contextPath}/api/getPhone.htm?id=${detail.id }">点击获取手机号</a>
		</div>
	</c:if>
	<c:if test="${!empty detail.phone }">
		<blockquote class="layui-elem-quote">
			手机号：<span class="layui-badge">${detail.phone }</span>
		</blockquote>
		<c:if test="${empty detail.smscode }">
			<div style="text-align: center;" id="sms_code_div">
				<a class="layui-btn" href="#" id="btn_smscode"
					onclick="getsmscode(${detail.id });">点击获取验证码</a>
			</div>
			<br>
			<p style="color: #00FF00;">获取短信根据系统返回时间决定，系统请求中请勿进行其他操作。</p>
			<br>
			<p style="color: red;">
				Step①:在微信中更换手机号<br> Step②:此页面点击获取验证码<br> Step③:执行其他操作
			</p>
			<p style="color: red;">请大家先吧手机号码在微信更换手机哪里！先提交！在点击获取验证码！</p>
			<p style="color: red;">对于收不到的验证码！系统会自动拉黑！然后在重新获取手机！ 大家请勿着急！系统有延迟！
			</p>
		</c:if>
		<c:if test="${!empty detail.smscode }">
			<blockquote class="layui-elem-quote">
				验证码：<span class="layui-badge">${detail.smscode }</span>
			</blockquote>
			<c:if test="${empty detail.overtime}">
				<p style="color: red;margin: 0 auto;text-align: center;">
					<b>重要提示！出现那种情况请一一对应点击相应按钮！请勿乱点！发现将不结账！<b>
				</p>
				<a style="width: 100%;height: 60px;font-size: 42px;" class="layui-btn layui-btn-normal"
					href="${pageContext.request.contextPath}/api/finishtask.htm?id=${detail.id }">完成任务</a>
				<p style="color: red;margin: 0 auto;text-align: center;">绑定完成 请点击此按钮↑↑↑↑↑↑↑</p>
				<br>
				<a style="width: 100%;height: 60px;font-size: 42px;" class="layui-btn layui-btn-warm"
					href="${pageContext.request.contextPath}/api/doagain.htm?id=${detail.id }">已被绑定</a>
				<p style="color: red;margin: 0 auto;text-align: center;">出现已被其他微信号绑定！请点击此按钮↑↑↑↑↑↑↑</p>
				<br>
				<a style="width: 100%;height: 60px;font-size: 42px;" class="layui-btn layui-btn-danger"
					href="${pageContext.request.contextPath}/api/domore.htm?id=${detail.id }">操作频繁</a>
				<p style="color: red;margin: 0 auto;text-align: center;">提交验证出现操作频繁，请点击此按钮↑↑↑↑↑↑↑</p>
			</c:if>
			<c:if test="${!empty detail.overtime}">
				<c:if test="${empty detail.suretime}">
					<p>等待管理员验收</p>
				</c:if>
			</c:if>
		</c:if>
	</c:if>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/layui.all.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/layer.js"></script>
	<script type="text/javascript">
		var InterValObj; //timer变量，控制时间 
		var count = 60; //间隔函数，1秒执行 
		var curCount;//当前剩余秒数 
		//timer处理函数 
		function SetRemainTime() {
			if (curCount == 0) {
				window.clearInterval(InterValObj);//停止计时器 
				$("#btn_smscode").text("提示：获取失败！此任务已被删除不计次！");
			} else {
				curCount--;
				$("#btn_smscode").text("请等待："+curCount + "秒可查看验证码！");
			}
		}

		function getsmscode(detailid) {
			curCount = count;
			$("#btn_smscode").attr("disabled", "true");
			$("#btn_smscode").removeAttr("class");
			$("#btn_smscode").removeAttr("onclick");
			$("#btn_smscode").attr("class", "layui-btn layui-btn-disabled");
			$("#btn_smscode").text("请等待："+curCount + "秒可查看验证码！");
			InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次 
			layer.load();
			$.ajax({
				type : "post",
				url : $("#ctx").val()+"/api/getSmscodeAsyc.htm",
				data : {id : detailid},
				dataType : "json",
				success : function(data) {
					layer.closeAll();
					location.reload();
				}
			});
		}
	</script>

</body>
</html>