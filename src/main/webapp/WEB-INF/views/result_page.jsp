<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/res/css/layer.css">
<Link Rel="SHORTCUT ICON" href="${pageContext.request.contextPath}/res/favicon.ico">
<title>返回结果状态确认</title>
</head>
<body>
	<h3><a style="color:#6495ED;" href="${pageContext.request.contextPath}/index.htm">主页</a>&nbsp;&nbsp;${title }</h3>
	
	<h3>${msg }</h3>
	 
	<script type="text/javascript" src="${pageContext.request.contextPath}/res/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/res/js/layer.js"></script>
</body>
</html>