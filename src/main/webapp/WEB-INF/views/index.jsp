<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>主页面</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/res/css/layui.css">
<Link Rel="SHORTCUT ICON" href="${pageContext.request.contextPath}/res/favicon.ico">
</head>
<body style="text-align: center;">
	<div style="text-align: center;">
		<button style="width: 260px;" class="layui-btn layui-btn-disabled">欢迎你:${user.mobile }</button>
	</div>
	<br>

	<div style="text-align: center;">
		<a style="width: 260px;" class="layui-btn"
			href="${pageContext.request.contextPath}/detail/mytask.htm">我的任务</a><br>
		<br> <a style="width: 260px;" class="layui-btn layui-btn-normal"
			href="${pageContext.request.contextPath}/detail/dotask.htm">继续任务</a><br>
		<br> <a style="width: 260px;" class="layui-btn layui-btn-warm"
			href="${pageContext.request.contextPath}/user/pwdpage.htm">更改密码</a><br>
		<br> <a style="width: 260px;" class="layui-btn layui-btn-danger"
			href="${pageContext.request.contextPath}/logout.htm">安全退出</a>
	</div>
	<p style="color: red;">请每天晚上八点前务必完成所有任务！！！</p>
	<p style="color: red;">此管理系统，只针对，老司机测试通过的用户而开放的，请勿在此系统测试新号！</p>
	<p style="color: red;">如需测试新号，请联系管理员！如果发现某人在此系统测试新号！将会永久拉黑!</p>
	<p style="color: red;">
		如有问题咨询qq1050495932（群①：638230980，群②：660652418）</p>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/res/js/layui.all.js"></script>
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?bc8a0435d3425b22c7be30e3d29fca07";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>

</body>
</html>